var express = require('express');
 var session=require('express-session');
 var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');

var app = express();

var index = require('./routes/index');
var users = require('./routes/users');

var passport = require('passport');
var flash = require('connect-flash');

require('./routes/passport')(passport); 



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cookieParser()); // read cookies (needed for auth)

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
	secret: 'vidyapathaisalwaysrunning',
	resave: true,
	saveUninitialized: true
 } )); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(express.static(path.join(__dirname, 'public')));

app.post('/login',
  passport.authenticate('local-login', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: true }),
        function(req, res) {
            console.log("hello");
        res.redirect('/');
    });
app.post('/signup',
  passport.authenticate('local-signup', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: true }),
        function(req, res) {
            console.log("hello");
        res.redirect('/');
    });

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
