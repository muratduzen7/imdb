var express = require('express');

var multer = require('multer');
var router = express.Router();
var mysql = require('mysql');
var connection = mysql.createConnection({
				  host     : 'localhost',
				  user     : 'root',
				  password : ''
				});

connection.query('USE vtproject');	

var path = require('path');
router.use(express.static(path.join(__dirname, 'public')));

function isadmin(req, res, next) {

    // do any checks you want to in here

    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
    if(req.user)
    {
    	if (req.user.admin)
        return next();
    }

    // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
    res.redirect('/');
}

/* GET home page. */
router.get('/', function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			connection.query("SELECT `fid`,AVG(`rates`.`rate`) AS `ort` FROM `rates` group by `fid` ORDER BY AVG(`rates`.`rate`) DESC LIMIT 5",function(err,rows){	
				var a="";
				 for(var i=0;i<rows.length;i++){
				 	if(i==0)
				 		a+=rows[i].fid;
				 	else
				 		a+=','+rows[i].fid;	
				 }
				 var orts=rows;
				  connection.query("SELECT * FROM `film` where `fid` IN ("+a+") ORDER BY FIELD(fid,"+a+")",function(err,rows){	
				  var topfivefilm=rows;
								 connection.query("SELECT `pid`,AVG(`ratesperson`.`rate`) AS `ort` FROM `ratesperson` group by `pid` ORDER BY AVG(`ratesperson`.`rate`) DESC LIMIT 5",function(err,rows){	
								var a="";
								 for(var i=0;i<rows.length;i++){
								 	if(i==0)
								 		a+=rows[i].pid;
								 	else
								 		a+=','+rows[i].pid;	
								 }
								 var ortsperson=rows;
								  connection.query("SELECT * FROM `person` where `pid` IN ("+a+") ORDER BY FIELD(pid,"+a+")",function(err,rows){	
								  var topfiveperson=rows;
								  				connection.query("SELECT DISTINCT `pid` FROM `cast` WHERE `jid` = 1",function(err,rows){	
												  var a="";
													 for(var i=0;i<rows.length;i++){
													 	if(i==0)
													 		a+=rows[i].pid;
													 	else
													 		a+=','+rows[i].pid;	
													 }
												  				 connection.query("SELECT `pid`,AVG(`ratesperson`.`rate`) AS `ort` FROM `ratesperson` WHERE `pid` IN ("+a+") group by `pid` ORDER BY AVG(`ratesperson`.`rate`) DESC LIMIT 5",function(err,rows){	
																	var a="";
																	 for(var i=0;i<rows.length;i++){
																	 	if(i==0)
																	 		a+=rows[i].pid;
																	 	else
																	 		a+=','+rows[i].pid;	
																	 }
																	 var ortsdirector=rows;
																	  connection.query("SELECT * FROM `person` where `pid` IN ("+a+") ORDER BY FIELD(pid,"+a+")",function(err,rows){	
																				var topfivedirector=rows;
																										  connection.query("SELECT * FROM `type`",function(err,rows){	
																									var cat=rows;
																										  res.render('index', { title: 'Express' ,cat:cat,orts:orts,ortsperson:ortsperson,ortsdirector:ortsdirector,topfivedirector:topfivedirector,topfiveperson:topfiveperson,topfivefilm:topfivefilm,userid:userid, isadmin:isadmin });
																										 
																						  });
																	  });
																  });

												  });
								  });
							  });
				  });
			  });
});


router.get('/film/:filmId', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
        connection.query("SELECT * FROM `film` WHERE `fid` = "+req.params.filmId+";",function(err,rows){	
        if(rows.length>0)
        {
        var filmname=rows[0].name;
		var filmid=rows[0].fid;
		var description=rows[0].description;
		var photo=rows[0].photo;
		var gallery=rows[0].gallery.split(',');
		

				connection.query("select * from cast where fid = '"+req.params.filmId+"'",function(err,rows){
				var casts=rows;	
				var castpid="pid='"+rows[0].pid+"' ";
					for(var x=1;x<rows.length;x++)	
					{
						castpid=castpid+"or pid='"+rows[x].pid+"' ";
					}
				var castjid="jid='"+rows[0].jid+"' ";
					for(var x=1;x<rows.length;x++)	
					{
						castjid=castjid+"or jid='"+rows[x].jid+"' ";
					}
					console.log("1");
					connection.query("select * from person where "+castpid+"",function(err,rows){
					var persons=rows;
					
								connection.query("select * from job where "+castjid+"",function(err,rows){
								var jobs=rows;
											console.log("2");
											connection.query("select * from commentfilm where fid='"+req.params.filmId+"'",function(err,rows){
											if(rows.length>0)
											{
											var comments=rows;
											var commentsuid="id='"+rows[0].uid+"' ";
											}
												for(var x=1;x<rows.length;x++)	
												{
													commentsuid=commentsuid+"or id='"+rows[x].uid+"' ";
												}
													connection.query("select id,name,photo from users where "+commentsuid+"",function(err,rows){
												var commentsuser=rows;
												
													connection.query("select * from likes where fid='"+req.params.filmId+"' and uid='"+userid+"'",function(err,rows){
													if(rows.length)
													{
														var liked=1;
													}
													else
													{
														var liked=0;
													}
														console.log("3");
														connection.query("select avg(rate) as r from rates where fid='"+req.params.filmId+"' group by fid",function(err,rows){
															if(rows.length>0)
															{
														rate=parseInt(rows[0].r);	
															}
															else
															{
																rate=0;
															}
															console.log("5");
													 res.render('film', { userid:userid, liked:liked,gallery:gallery,filmid:filmid,rate:rate,isadmin:isadmin,commentsuser:commentsuser,comments:comments,filmname:filmname,photo:photo,description:description,casts:casts,jobs:jobs,persons:persons,title: 'Express' });
														});
													});
											});
												
									});
								
							});
					});
			});
		}
		});
});
router.post('/film/comment/:filmId/:userid', function(req, res, next) {
		connection.query("INSERT INTO `commentfilm` (`commentid`, `uid`, `fid`, `comment`) VALUES (NULL, '"+req.params.userid+"', '"+req.params.filmId+"', '"+req.body.comment+"');",function(err,rows){	
			
			  res.redirect('/film/'+req.params.filmId);
			  });

});
router.post('/person/comment/:personId/:userid', function(req, res, next) {
		connection.query("INSERT INTO `commentperson` (`commentpid`, `uid`, `pid`, `comment`) VALUES (NULL, '"+req.params.userid+"', '"+req.params.personId+"', '"+req.body.comment+"');",function(err,rows){	
			
			  res.redirect('/person/'+req.params.personId);
			  });

});
router.get('/film/rate/:ratepoint/:filmId/:userid', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
if(userid)
{
		connection.query("INSERT INTO `rates` (`rid`, `uid`, `fid`, `rate`) VALUES (NULL, '"+req.params.userid+"', '"+req.params.filmId+"', '"+req.params.ratepoint+"');",function(err,rows){	
			
			  res.redirect('/film/'+req.params.filmId);
			  });
}
else
{
	 res.redirect('/login');
}

});
router.get('/person/rate/:ratepoint/:personId/:userid', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
if(userid)
{
		connection.query("INSERT INTO `ratesperson` (`rpid`, `uid`, `pid`, `rate`) VALUES (NULL, '"+req.params.userid+"', '"+req.params.personId+"', '"+req.params.ratepoint+"');",function(err,rows){	
			
			  res.redirect('/person/'+req.params.personId);
			  });
}
else
{
	 res.redirect('/login');
}

});
router.get('/film/like/:filmId/:userid', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
if(userid)
{
		connection.query("INSERT INTO `likes` (`lid`, `fid`, `uid`) VALUES (NULL, '"+req.params.filmId+"', '"+req.params.userid+"');",function(err,rows){	
			
			  res.redirect('/film/'+req.params.filmId);
			  });
}
else
{
	 res.redirect('/login');
}

});
router.get('/film/unlike/:filmId/:userid/', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
if(userid)
{
		connection.query("DELETE FROM `likes` WHERE `likes`.`fid` = "+req.params.filmId+" AND `likes`.`uid` = "+req.params.userid+"",function(err,rows){	
			
			  res.redirect('/film/'+req.params.filmId);
			  });
}
else
{
	 res.redirect('/login');
}

});
router.get('/person/like/:personId/:userid', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
if(userid)
{
		connection.query("INSERT INTO `likesperson` (`lpid`, `uid`, `pid`) VALUES (NULL, '"+req.params.userid+"', '"+req.params.personId+"');",function(err,rows){	
			
			  res.redirect('/person/'+req.params.personId);
			  });
}
else
{
	 res.redirect('/login');
}

});
router.get('/person/unlike/:personId/:userid/', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
if(userid)
{
		connection.query("DELETE FROM `likesperson` WHERE `likesperson`.`pid` = "+req.params.personId+" AND `likesperson`.`uid` = "+req.params.userid+"",function(err,rows){	
			
			  res.redirect('/person/'+req.params.personId);
			  });
}
else
{
	 res.redirect('/login');
}

});

router.get('/person/:personId', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
	connection.query("SELECT * FROM `person` WHERE `pid` ="+req.params.personId+" ",function(err,rows){	
			if(rows.length>0)
			{
				var name=rows[0].name;
				var description=rows[0].description;
				var personid=rows[0].pid;
				var photo=rows[0].photo;
				connection.query("SELECT * FROM `cast` WHERE `pid` ="+req.params.personId+" ",function(err,rows){	
						if(rows.length>0)
						{
								var f="";
								var j="";
								 for(var i=0;i<rows.length;i++){
								 	if(i==0)
								 	{
								 		f+=rows[i].fid;
								 		j+=rows[i].jid;
								 	}
								 	else
								 	{
								 		f+=','+rows[i].fid;	
								 		j+=','+rows[i].jid;	
								 	}
								 }
								  connection.query("SELECT fid,name FROM `film` where `fid` IN ("+f+") ORDER BY FIELD(fid,"+f+")",function(err,rows){	
								  var filmdizisi=rows;
											 	connection.query("SELECT name FROM `job` where `jid` IN ("+j+") ORDER BY FIELD(jid,"+j+")",function(err,rows){	
										  var jobdizisi=rows;
														  			connection.query("select * from commentperson where pid='"+req.params.personId+"'",function(err,rows){
																		if(rows.length>0)
																		{
																		var comments=rows;
																		var commentsuid="id='"+rows[0].uid+"' ";
																		}
																			for(var x=1;x<rows.length;x++)	
																			{
																				commentsuid=commentsuid+"or id='"+rows[x].uid+"' ";
																			}
																				connection.query("select id,name,photo from users where "+commentsuid+"",function(err,rows){
																			var commentsuser=rows;
																			
																				connection.query("select * from likesperson where pid='"+req.params.personId+"' and uid='"+userid+"'",function(err,rows){
																				if(rows.length)
																				{
																					var liked=1;
																				}
																				else
																				{
																					var liked=0;
																				}
																					console.log("3");
																					connection.query("select avg(rate) as r from ratesperson where pid='"+req.params.personId+"' group by pid",function(err,rows){
																						if(rows.length>0)
																						{
																					rate=parseInt(rows[0].r);	
																						}
																						else
																						{
																							rate=0;
																						}
																						console.log("5");
															  res.render('person', { title: 'Express', comments:comments, commentsuser:commentsuser, liked:liked, rate:rate, jobdizisi:jobdizisi, filmdizisi:filmdizisi, photo:photo, personid:personid, userid:userid, name:name, description:description });
														  							});
														  					});
																	});
																});


														  });
										  });
						}
				});
			}
	});
  
});
router.get('/ara/:searchkey', function(req, res, next) {
  res.render('ara', { title: 'Express' });
});
router.post('/ara', function(req, res, next) {
if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;
}
else
{
	var userid=0;
}
	connection.query("select * from film where name like '%"+req.body.searchkey+"%'",function(err,rows){	
			var searchfilm=rows;
					connection.query("select * from person where name like '%"+req.body.searchkey+"%'",function(err,rows){	
				var searchperson=rows;
					
				  res.render('ara', { title: 'Express' , isadmin:isadmin, userid:userid,searchfilm:searchfilm,searchperson:searchperson ,searchkey:req.body.searchkey });
				});
			});
});
router.get('/login', function(req, res, next) {

if (typeof req.user !== 'undefined') {
var userid=req.user.id;
var isadmin=req.user.admin;

connection.query("SELECT * FROM `users` WHERE `id` = "+userid+"",function(err,rows){	
			var name=rows[0].name;
			var photo=rows[0].photo;	
				 connection.query("SELECT * FROM `film` WHERE `fid` IN (SELECT `fid` FROM `likes` WHERE `uid` = "+userid+")",function(err,rows){	
				if(rows.length>0)
				{
					var filmnames=rows;	
				}
				else
				{
					var filmnames=0;
				}
					connection.query("SELECT DISTINCT `pid` FROM `cast` WHERE `jid` = 1",function(err,rows){	
												  var a="";
													 for(var i=0;i<rows.length;i++){
													 	if(i==0)
													 		a+=rows[i].pid;
													 	else
													 		a+=','+rows[i].pid;	
													 }
					 connection.query("SELECT * FROM `person` WHERE `pid` IN (SELECT `pid` FROM `likesperson` WHERE `pid` NOT IN ("+a+") AND `uid` = "+userid+")",function(err,rows){	
					if(rows.length>0)
					{
						var personnames=rows;	
					}
					else
					{
						var personnames=0;
					}

					 			connection.query("SELECT * FROM `person` WHERE `pid` IN (SELECT `pid` FROM `likesperson` WHERE `pid` IN ("+a+") AND`uid` = "+userid+")",function(err,rows){	
								if(rows.length>0)
								{
									var directornames=rows;	
								}
								else
								{
									var directornames=0;
								}
								 res.render('login', 
								  	{ title: 'Express', directornames:directornames, personnames:personnames, filmnames:filmnames, userid:userid, photo:photo, name:name, isadmin:isadmin }

								  );
								});
							});
					});
				});
			});

}
else
{
	var userid=0;
	 res.render('login', 
  	{ title: 'Express', userid:userid, isadmin:isadmin }

  );
}
 
 
});

router.get('/signup', function(req, res, next) {


	 res.render('signup', 
  	{ title: 'Express'}

  );

 
 
});

router.get('/logout', function (req, res){
  req.session.destroy(function (err) {
    res.redirect('/'); //Inside a callback… bulletproof!
  });
});

router.get('/me', function(req, res, next) {
  res.render('profile', { title: 'Express' });
});

router.get('/types/edit', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			 connection.query("select * from type",function(err,rows){	
			var types=rows;
				
			  res.render('turekle', { title: 'Express' ,userid:userid, isadmin:isadmin ,types:types });
			});
});

router.post('/types/add',isadmin, function(req, res, next) {
	
			 connection.query("INSERT INTO `type` (`tid`, `name`) VALUES (NULL, '"+req.body.type+"');",function(err,rows){	
			
			  res.redirect('/types/edit');
			  });
});

router.get('/types/delete/:id',isadmin, function(req, res, next) {
	
			 connection.query("DELETE FROM `type` WHERE `type`.`tid` = "+req.params.id+"",function(err,rows){	
			
			  res.redirect('/types/edit');
			  });
});

//JOB
router.get('/jobs/edit', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			 connection.query("select * from job",function(err,rows){	
			var jobs=rows;
				
			  res.render('gorevekle', { title: 'Express' ,userid:userid, isadmin:isadmin ,jobs:jobs });
			});
});

router.post('/jobs/add',isadmin, function(req, res, next) {
	
			 connection.query("INSERT INTO `job` (`jid`, `name`) VALUES (NULL, '"+req.body.type+"');",function(err,rows){	
			
			  res.redirect('/jobs/edit');
			  });
});

router.get('/jobs/delete/:id',isadmin, function(req, res, next) {
	
			 connection.query("DELETE FROM `job` WHERE `job`.`jid` = "+req.params.id+"",function(err,rows){	
			
			  res.redirect('/jobs/edit');
			  });
});

//FİLM
router.get('/films/edit', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			 connection.query("select * from film",function(err,rows){	
			var films=rows;
				
					 connection.query("select * from type",function(err,rows){	
					var types=rows;
						
					  res.render('filmekle', { title: 'Express' ,userid:userid, isadmin:isadmin ,films:films ,types:types });
					});
			});
});

router.post('/films/add',isadmin, function(req, res, next) {
			
			var st=req.body.types[0];
			console.log(".1.."+st+req.body.types.length);
			for(var x=1;x<req.body.types.length;x++)
			{
				console.log(".2.."+st+req.body.types.length);
				st=st+","+req.body.types[x];

			}
			console.log(".3.."+st);
			 connection.query("INSERT INTO `film` (`fid`, `name`, `description`, `type`, `date`, `trailer`, `photo`, `rate`) VALUES (NULL, '"+req.body.filmname+"', '"+req.body.filmdescription+"', '"+st+"', '', '', '', '');",function(err,rows){	
			
			  res.redirect('/films/edit');
			  });
});

router.get('/films/delete/:id',isadmin, function(req, res, next) {
	
			 connection.query("DELETE FROM `film` WHERE `film`.`fid` = "+req.params.id+"",function(err,rows){	
			
			  res.redirect('/films/edit');
			  });
});

router.get('/category/:id', function(req, res, next) {
	
			 connection.query("SELECT * FROM `film` WHERE `film`.`type` LIKE '%"+req.params.id+"%'",function(err,rows){	
					var cat=rows;
														connection.query("SELECT * FROM `type`",function(err,rows){	
																									var types=rows;
																												connection.query("SELECT name FROM `type` WHERE `tid`="+req.params.id+" ",function(err,rows){	
																											var categoryname=rows[0].name;
																												res.render('category', { title: 'Express' ,categoryname:categoryname,cat:cat,types:types });																										 
																								  });																			 
																						  });
							  
			  });
});

router.get('/films/cast/:id', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			 connection.query("select * from cast where fid='"+req.params.id+"'",function(err,rows){	
			var casts=rows;
				
					 connection.query("select * from person ",function(err,rows){	
					var persons=rows;
						
						 connection.query("select * from job ",function(err,rows){	
						var jobs=rows;
							
								connection.query("select * from film where fid="+req.params.id,function(err,rows){	
							var filmname=rows[0].name;
								
							  res.render('castekle', { title: 'Express' ,userid:userid, isadmin:isadmin,filmname:filmname, filmid:req.params.id ,casts:casts, persons:persons , jobs:jobs });
							});
						  
						 });
					});
			});
});
router.get('/films/images/:id', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			connection.query("select * from film where fid = '"+req.params.id+"'",function(err,rows){	
				var filmname=rows[0].name;
				var photo=rows[0].photo;
							var gallery=rows[0].gallery;
							var galleryimage=gallery.split(',');
					res.render('filmgorselekle', { title: 'Express',isadmin:isadmin,userid:userid ,galleryimage:galleryimage,filmid:req.params.id ,filmname:filmname,photo:photo});
		});
							
});
router.get('/films/gallerydelete/:id/:photoname', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			connection.query("SELECT * FROM film where fid = '"+req.params.id+"'",function(err,rows){	
							var gallery=rows[0].gallery;
							var galleryimage=gallery.split(',');
							var galleryimage2=[];
							for(var i=0;i<galleryimage.length;i++)
							{
								if(galleryimage[i].toString().trim()===req.params.photoname)
								{
									console.log(req.params.photoname+"-");
								}
								else
								{
									galleryimage2.push(galleryimage[i]);
								}
							}
							gallery=galleryimage2.join(",");
							console.log("-----------"+gallery);
										connection.query("UPDATE film SET gallery='"+gallery+"' WHERE fid = '"+req.params.id+"'",function(err,rows){	
											res.redirect('/films/images/'+req.params.id);
					});
		});
							
});
router.get('/persons/images/:id', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			connection.query("select * from person where pid = '"+req.params.id+"'",function(err,rows){	
				var personname=rows[0].name;
				var photo=rows[0].photo;
							
					res.render('persongorselekle', { title: 'Express',isadmin:isadmin,userid:userid ,personid:req.params.id ,personname:personname,photo:photo});
		});
							
});
router.get('/types/images/:id', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			connection.query("select * from type where tid = '"+req.params.id+"'",function(err,rows){	
				var typename=rows[0].name;
				var photo=rows[0].photo;
							
					res.render('typegorselekle', { title: 'Express',isadmin:isadmin,userid:userid ,typeid:req.params.id ,typename:typename,photo:photo});
		});
							
});
router.post('/films/images/:id', isadmin, multer({ dest: './public/uploads/'}).single('upl'), function(req,res){
	if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
	console.log(req.body); //form fields
	/* example output:
	{ title: 'abc' }
	 */
	console.log(req.file); //form files
	/* example output:
            { fieldname: 'upl',
              originalname: 'grumpy.png',
              encoding: '7bit',
              mimetype: 'image/png',
              destination: './uploads/',
              filename: '436ec561793aa4dc475a88e84776b1b9',
              path: 'uploads/436ec561793aa4dc475a88e84776b1b9',
              size: 277056 }
	 */
	 connection.query("UPDATE `film` SET `photo` = '"+req.file.filename+"' WHERE `film`.`fid` = '"+req.params.id+"';",function(err,rows){	
			
								 res.redirect('/films/images/'+req.params.id);
			  });
});
router.post('/types/images/:id', isadmin, multer({ dest: './public/uploads/'}).single('upl'), function(req,res){
	if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
	console.log(req.body); //form fields
	/* example output:
	{ title: 'abc' }
	 */
	console.log(req.file); //form files
	/* example output:
            { fieldname: 'upl',
              originalname: 'grumpy.png',
              encoding: '7bit',
              mimetype: 'image/png',
              destination: './uploads/',
              filename: '436ec561793aa4dc475a88e84776b1b9',
              path: 'uploads/436ec561793aa4dc475a88e84776b1b9',
              size: 277056 }
	 */
	 connection.query("UPDATE `type` SET `photo` = '"+req.file.filename+"' WHERE `type`.`tid` = '"+req.params.id+"';",function(err,rows){	
			
								 res.redirect('/types/images/'+req.params.id);
			  });
});
router.post('/persons/images/:id', isadmin, multer({ dest: './public/uploads/'}).single('upl'), function(req,res){
	if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
	console.log(req.body); //form fields
	/* example output:
	{ title: 'abc' }
	 */
	console.log(req.file); //form files
	/* example output:
            { fieldname: 'upl',
              originalname: 'grumpy.png',
              encoding: '7bit',
              mimetype: 'image/png',
              destination: './uploads/',
              filename: '436ec561793aa4dc475a88e84776b1b9',
              path: 'uploads/436ec561793aa4dc475a88e84776b1b9',
              size: 277056 }
	 */
	 connection.query("UPDATE `person` SET `photo` = '"+req.file.filename+"' WHERE `person`.`pid` = '"+req.params.id+"';",function(err,rows){	
			
								 res.redirect('/persons/images/'+req.params.id);
			  });
});
router.post('/profile/images/:id', multer({ dest: './public/uploads/'}).single('upl'), function(req,res){
	if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
	console.log(req.body); //form fields
	/* example output:
	{ title: 'abc' }
	 */
	console.log(req.file); //form files
	/* example output:
            { fieldname: 'upl',
              originalname: 'grumpy.png',
              encoding: '7bit',
              mimetype: 'image/png',
              destination: './uploads/',
              filename: '436ec561793aa4dc475a88e84776b1b9',
              path: 'uploads/436ec561793aa4dc475a88e84776b1b9',
              size: 277056 }
	 */
	 connection.query("UPDATE `users` SET `photo` = '"+req.file.filename+"' WHERE `users`.`id` = '"+req.params.id+"';",function(err,rows){	
			
								 res.redirect('/login');
			  });
});
router.post('/films/imagesgallery/:id', isadmin, multer({ dest: './public/uploads/'}).single('upl'), function(req,res){
	if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
	console.log(req.body); //form fields
	/* example output:
	{ title: 'abc' }
	 */
	console.log(req.file); //form files
	/* example output:
            { fieldname: 'upl',
              originalname: 'grumpy.png',
              encoding: '7bit',
              mimetype: 'image/png',
              destination: './uploads/',
              filename: '436ec561793aa4dc475a88e84776b1b9',
              path: 'uploads/436ec561793aa4dc475a88e84776b1b9',
              size: 277056 }
	 */
	 connection.query("select * from film where fid = '"+req.params.id+"'",function(err,rows){	
	 	var gallery=rows[0].gallery;
	 	if(gallery=="")
	 	{

	 	}
	 	else
	 	{
	 		gallery=gallery+",";
	 	}
	 connection.query("UPDATE `film` SET `gallery` = '"+gallery+req.file.filename+"' WHERE `film`.`fid` = '"+req.params.id+"';",function(err,rows){	
			
								
								 res.redirect('/films/images/'+req.params.id);
					});
			  });
});

router.post('/films/cast/add/:id', isadmin, function(req, res, next) {
			connection.query("INSERT INTO `cast` (`cid`, `fid`, `pid`, `jid`) VALUES (NULL, '"+req.params.id+"', '"+req.body.person+"', '"+req.body.job+"');",function(err,rows){	
			
			  res.redirect('/films/cast/'+req.params.id);
			  });
});
router.get('/films/cast/delete/:fid/:cid',isadmin, function(req, res, next) {
	
			 connection.query("DELETE FROM `cast` WHERE `cast`.`cid` = "+req.params.cid+"",function(err,rows){	
			
			  res.redirect('/films/cast/'+req.params.fid);
			  });
});

//PERSON
router.get('/persons/edit', isadmin, function(req, res, next) {
			if (typeof req.user !== 'undefined') {
			var userid=req.user.id;
			var isadmin=req.user.admin;
			}
			else
			{
				var userid=0;
			}
			 connection.query("select * from person",function(err,rows){	
			var persons=rows;
					  res.render('kisiekle', { title: 'Express' ,userid:userid, isadmin:isadmin ,persons:persons });
				
			});
});

router.post('/persons/add',isadmin, function(req, res, next) {
	
			 connection.query("INSERT INTO `person` (`pid`, `name`, `born`, `description`, `rate`) VALUES (NULL, '"+req.body.name+"',NULL,'"+req.body.description+"',NULL);",function(err,rows){	
			
			  res.redirect('/persons/edit');
			  });
});

router.get('/persons/delete/:id',isadmin, function(req, res, next) {
	
			 connection.query("DELETE FROM `person` WHERE `person`.`pid` = "+req.params.id+"",function(err,rows){	
			
			  res.redirect('/persons/edit');
			  });
});










module.exports = router;
