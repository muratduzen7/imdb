-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 10 Oca 2017, 11:31:47
-- Sunucu sürümü: 10.1.19-MariaDB
-- PHP Sürümü: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `vtproject`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `cast`
--

CREATE TABLE `cast` (
  `cid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `jid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `cast`
--

INSERT INTO `cast` (`cid`, `fid`, `pid`, `jid`) VALUES
(13, 10, 8, 9),
(14, 10, 9, 8),
(15, 10, 10, 8),
(16, 10, 11, 8),
(17, 10, 7, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `cat`
--

CREATE TABLE `cat` (
  `catid` int(11) NOT NULL,
  `tid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `commentfilm`
--

CREATE TABLE `commentfilm` (
  `commentid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `commentfilm`
--

INSERT INTO `commentfilm` (`commentid`, `uid`, `fid`, `comment`) VALUES
(1, 1, 7, 'yorumdksd'),
(2, 1, 7, 'yorumdksdfgdfgd'),
(3, 1, 7, 'sdsafmlsdkgsdf'),
(4, 1, 7, 'sonyorum'),
(5, 1, 7, 'yorum2'),
(6, 1, 10, 'hghgh');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `film`
--

CREATE TABLE `film` (
  `fid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trailer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery` text COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(1,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `film`
--

INSERT INTO `film` (`fid`, `name`, `description`, `type`, `date`, `trailer`, `photo`, `gallery`, `rate`) VALUES
(10, 'The Fate of the Furious (2017)', 'When a mysterious woman seduces Dom into the world of crime and a betrayal of those closest to him, the crew face trials that will test them as never before.', '11,17', '', '', '475b2bf3e12f9df010c1ec42708a4113', 'e3dcef3fc96b4ce04f22b554e694dc59,5e76e604aad0aa6aa473b3964da98e47', '0.0');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `job`
--

CREATE TABLE `job` (
  `jid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `job`
--

INSERT INTO `job` (`jid`, `name`) VALUES
(1, 'Director'),
(8, 'Stars'),
(9, 'Writers');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `likes`
--

CREATE TABLE `likes` (
  `lid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `likes`
--

INSERT INTO `likes` (`lid`, `uid`, `fid`) VALUES
(3, 1, 7),
(4, 1, 10),
(5, 1, 10),
(6, 1, 10);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `person`
--

CREATE TABLE `person` (
  `pid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `born` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` decimal(1,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `person`
--

INSERT INTO `person` (`pid`, `name`, `born`, `description`, `rate`) VALUES
(7, 'F. Gary Gray', NULL, 'F. Gary Gray was born on July 17, 1969 in New York City, New York, USA as Felix Gary Gray. He is a director and actor, known for Italyan Isi (2003), Adalet pesinde (2009) and Straight Outta Compton (2015).', NULL),
(8, 'Chris Morgan', NULL, 'Chris Morgan is a writer and producer', NULL),
(9, 'Dwayne Johnson', NULL, 'Dwayne Douglas Johnson, also known as The Rock, was born on May 2, 1972 in Hayward, California, to Ata Johnson (born Feagaimaleata Fitisemanu Maivia) and Canadian-born professional wrestler Rocky Johnson (born Wayde Douglas Bowles).', NULL),
(10, 'Charlize Theron', NULL, 'Charlize Theron was born in Benoni, a city in the greater Johannesburg-area, in South Africa, the only child of Gerda Theron (née Gerda Jacoba Aletta Maritz) and Charles Jacobus Theron.', NULL),
(11, 'Jason Statham', NULL, 'Jason Statham was born in Shirebrook, Derbyshire, to Eileen (Yates), a dancer, and Barry Statham, a street merchant and lounge singer. Statham has done quite a lot in a short time.', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `type`
--

CREATE TABLE `type` (
  `tid` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `type`
--

INSERT INTO `type` (`tid`, `name`) VALUES
(11, 'Action'),
(12, 'Comedi'),
(13, 'Drama'),
(14, 'Romantic'),
(15, 'Family'),
(16, 'Horror'),
(17, 'Adventure'),
(18, 'porno');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `admin`, `photo`) VALUES
(1, 'murat düzen', 'muratdzn@gmail.com', '12345', 1, 'https://lh3.googleusercontent.com/-I7iquRG9nFc/AAAAAAAAAAI/AAAAAAAAAAA/AKB_U8s-NHX-7rrZ71WNFVIJClZpmOaf0Q/s32-c-mo/photo.jpg');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `cast`
--
ALTER TABLE `cast`
  ADD PRIMARY KEY (`cid`);

--
-- Tablo için indeksler `cat`
--
ALTER TABLE `cat`
  ADD PRIMARY KEY (`catid`);

--
-- Tablo için indeksler `commentfilm`
--
ALTER TABLE `commentfilm`
  ADD PRIMARY KEY (`commentid`);

--
-- Tablo için indeksler `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`fid`);

--
-- Tablo için indeksler `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`jid`);

--
-- Tablo için indeksler `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`lid`);

--
-- Tablo için indeksler `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`pid`);

--
-- Tablo için indeksler `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`tid`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `cast`
--
ALTER TABLE `cast`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Tablo için AUTO_INCREMENT değeri `cat`
--
ALTER TABLE `cat`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `commentfilm`
--
ALTER TABLE `commentfilm`
  MODIFY `commentid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Tablo için AUTO_INCREMENT değeri `film`
--
ALTER TABLE `film`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Tablo için AUTO_INCREMENT değeri `job`
--
ALTER TABLE `job`
  MODIFY `jid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Tablo için AUTO_INCREMENT değeri `likes`
--
ALTER TABLE `likes`
  MODIFY `lid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Tablo için AUTO_INCREMENT değeri `person`
--
ALTER TABLE `person`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Tablo için AUTO_INCREMENT değeri `type`
--
ALTER TABLE `type`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
